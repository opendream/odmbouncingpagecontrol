//
//  ODMViewController.m
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

#import "ODMViewController.h"
#import "ODMPageControlViewCell.h"

@interface ODMViewController () <UIScrollViewDelegate>

@end

@implementation ODMViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	ODMPageControlViewCell *cell1 = [ODMPageControlViewCell cellWithIdentifier:nil title:@"Contact" icon:[UIImage imageNamed:@"contact-button"]];
	ODMPageControlViewCell *cell2 = [ODMPageControlViewCell cellWithIdentifier:nil title:@"Friend" icon:[UIImage imageNamed:@"friend-button"]];
	ODMPageControlViewCell *cell3 = [ODMPageControlViewCell cellWithIdentifier:nil title:@"Information" icon:[UIImage imageNamed:@"information-button"]];
	ODMPageControlViewCell *cell4 = [ODMPageControlViewCell cellWithIdentifier:nil title:@"Review" icon:[UIImage imageNamed:@"review-button"]];
	
	[pageControl setContents:@[cell1, cell2, cell3, cell4]];
	[pageControl setCurrentValue:0];
	[pageControl addTarget:self
					action:@selector(handlePageControlChanged:)
		  forControlEvents:UIControlEventValueChanged];


	[self.scrollView setContentSize:
	 CGSizeMake(CGRectGetWidth(self.view.bounds) * pageControl.contents.count
				, CGRectGetHeight(self.view.bounds))];
	
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[pageControl pageControlWillBeginDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[pageControl pageControlDidScroll:scrollView];
}

- (IBAction)nextPage:(id)sender
{
	pageControl.currentValue += 1;
}

- (IBAction)previousPage:(id)sender
{
	pageControl.currentValue -= 1;
}

- (void)handlePageControlChanged:(id)sender
{

}

@end
