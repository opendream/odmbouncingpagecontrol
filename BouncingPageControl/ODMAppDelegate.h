//
//  ODMAppDelegate.h
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ODMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
