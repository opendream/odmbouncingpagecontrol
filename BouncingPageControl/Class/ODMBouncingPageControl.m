//
//  ODMBouncingPageControl.m
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

#import "ODMBouncingPageControl.h"
#import "ODMPageControlViewCell.h"

@implementation ODMBouncingPageControl {
	CGPoint beginTrackingPoint;
	CGPoint startingScrollOffset;
	CGFloat startValue;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
		[self setClipsToBounds:YES];
		[self setUserInteractionEnabled:YES];
	}
	
	return self;
}

- (void)dealloc
{
	//
	// Release all cells
	//
	[self.contents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		obj = nil;
	}];
}

- (void)setContents:(NSArray *)contents
{
	//
	// Clear All Views
	//
	[_contents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[obj removeFromSuperview];
	}];

	_contents = contents;

	//
	// Add views
	//
	[_contents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[obj setFrame:self.bounds];
		[self addSubview:obj];
	}];

	_minValue = 0;
	_maxValue = (CGFloat)[_contents count] - 1;
}

- (void)setCurrentValue:(CGFloat)currentValue
{
	[self setCurrentValue:currentValue withAnimated:YES];
}

- (void)setCurrentValue:(CGFloat)currentValue withAnimated:(BOOL)animated
{
	currentValue = MAX(_minValue, currentValue);
	currentValue = MIN(_maxValue, currentValue);

	CGFloat width = CGRectGetWidth(self.bounds);
	CGFloat overlapCellWidth = [ODMPageControlViewCell headerSize].width / 2.0f;
	CGFloat bouncingFactor = 2 * overlapCellWidth * fmod(currentValue, 1.0f);

	//CGFloat nextValue = roundf(currentValue);
	//CGFloat specialEq = nextValue > 1 ? (fmod(currentValue, 1.0f) + 0.25f) / (nextValue - 1) : 0;
	//bouncingFactor = bouncingFactor * (int)specialEq;

	[UIView beginAnimations:@"CellAnimation" context:NULL];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:(animated ? 0.3f : 0)];
	[_contents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		CGRect f = [obj frame];
		NSInteger cellNumber = labs((NSInteger)currentValue - idx);
		ODMPageControlCellSide direction = currentValue < idx ? ODMPageControlCellSideRight : ODMPageControlCellSideLeft;

		//
		// CellNumber
		// 0 == Upcomming Page
		// 1 == Current Visible Page
		// 2 == Side Page
		// 3,4,5…
		if (cellNumber < 1) {
			if (currentValue >= 1.0f) {
				if (idx == ([_contents count] - 1)) {
					//
					// Last Page
					//
					f.origin.x = fmod(currentValue, 1.0f) * width; //+ overlapCellWidth;

				} else {
					//
					// Point to visible Cell
					//
					//f.origin.x = overlapCellWidth - bouncingFactor;
					f.origin.x = 0 - bouncingFactor;
				}
			} else {
				//
				// First page
				// No previous cell
				//
				// TODO
				// Adjust when move cell closer
				//
				f.origin.x = 0 - bouncingFactor;
			}

		} else if (direction == ODMPageControlCellSideLeft) {
			if (cellNumber == 1) {
				if (0 && currentValue >= 1.0f) {
					//
					// Half
					//
					f.origin.x = direction * overlapCellWidth - bouncingFactor * 1.5f;

				} else {
					//
					// Full
					// Only First Page
					//
					f.origin.x = direction * 3 * overlapCellWidth;
					//f.origin.x = direction * overlapCellWidth;
				}

			} else {
				//
				// Left Outbound
				//
				f.origin.x =  direction * width;
			}

		} else if (direction == ODMPageControlCellSideRight) {
			if (cellNumber == 1) {
				//
				// Full
				//
				f.origin.x = (1.0f - fmod(currentValue, 1.0f)) * (width - 3 * overlapCellWidth);

			} else if (cellNumber == 2) {
				//
				// Half
				//
				f.origin.x =  direction * (width -  overlapCellWidth) - bouncingFactor;

			} else if (cellNumber == 3 && currentValue < 1.0f) {
				//
				// Right Outbound
				//
				f.origin.x =  direction * (width + overlapCellWidth - bouncingFactor);
			} else {
				//
				// Right Outbound
				//
				f.origin.x =  direction * width;
			}

			//
			// Bring cells on the right to top
			//
			[self bringSubviewToFront:obj];
		}
		[obj setFrame:f];
	}];

	[UIView commitAnimations];
	_currentValue = currentValue;
}	

#pragma mark - Touch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];

	beginTrackingPoint = [[touches anyObject] locationInView:self];
	startValue = self.currentValue;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesMoved:touches withEvent:event];

	CGPoint location = [[touches anyObject] locationInView:self];
	CGFloat pointX = beginTrackingPoint.x - location.x;
	CGFloat percentageInView = (pointX / CGRectGetWidth(self.frame));
	self.currentValue = startValue + percentageInView;

	//NSLog(@"value %f", self.currentValue);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesEnded:touches withEvent:event];
	
	beginTrackingPoint = CGPointZero;
	startValue = self.currentValue;
}

- (void)pageControlWillBeginDragging:(UIScrollView *)scrollView
{
	startingScrollOffset = [scrollView contentOffset];
	startValue = self.currentValue;
}

- (void)pageControlDidScroll:(UIScrollView *)scrollView
{
	CGFloat pointX = scrollView.contentOffset.x - startingScrollOffset.x;
	CGFloat percentageInView = (pointX / CGRectGetWidth(self.bounds));
	self.currentValue = startValue + percentageInView;

	//NSLog(@"slide to value [%f] = %f", pointX, self.currentValue);
}

@end
