//
//  ODMBouncingPageControl.h
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum NSInteger {
	ODMPageControlCellSideLeft = -1,
	ODMPageControlCellSideRight = 1,
} ODMPageControlCellSide;

@interface ODMBouncingPageControl : UIControl

@property (nonatomic, strong) NSArray *contents;

@property (nonatomic, assign) CGFloat currentValue;
@property (nonatomic, assign) CGFloat minValue;
@property (nonatomic, assign) CGFloat maxValue;

- (void)pageControlWillBeginDragging:(UIScrollView *)scrollView;
- (void)pageControlDidScroll:(UIScrollView *)scrollView;

@end
