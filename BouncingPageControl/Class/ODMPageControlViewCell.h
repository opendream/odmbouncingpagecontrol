//
//  ODMPageControlViewCell.h
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

@interface ODMPageControlViewCell : UIView

@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundIconImageView;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

+ (ODMPageControlViewCell *)cellWithIdentifier:(NSString *)identifier;
+ (ODMPageControlViewCell *)cellWithIdentifier:(NSString *)identifier
										 title:(NSString *)title
										  icon:(UIImage *)icon
								  disabledIcon:(UIImage *)disabledIcon;

+ (CGSize)headerSize;

@end
