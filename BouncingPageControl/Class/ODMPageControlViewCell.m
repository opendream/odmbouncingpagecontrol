//
//  ODMPageControlViewCell.m
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

#import "ODMPageControlViewCell.h"

@implementation ODMPageControlViewCell

- (void)dealloc
{
	[self removeObserver:self
			  forKeyPath:@"frame"];
}

+ (CGSize)headerSize
{
	//
	// Icon size
	//
	return CGSizeMake(31, 31);
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	//
	// Rounded image view
	//
	_iconImageView.layer.cornerRadius = CGRectGetWidth(_iconImageView.frame) / 2.0f;

	//
	// Tracking x-position
	//
	[self addObserver:self
		   forKeyPath:@"frame"
			  options:NSKeyValueObservingOptionNew
			  context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"frame"]) {
		CGRect f = [change[@"new"] CGRectValue];
		CGFloat frameWidth = [[object superview] frame].size.width;
		CGFloat alphaFactor = (frameWidth - f.origin.x) / frameWidth;
		if (f.origin.x >= 0 && f.origin.x <= 240) {
			//
			// Show label
			//
			[[object titleLabel] setAlpha:alphaFactor];
			[self.iconImageView setAlpha:alphaFactor];

		} else if (f.origin.x <= 40 || f.origin.x >  240) {
			//
			// Hide label
			//
			[[object titleLabel] setAlpha:0];
			[self.iconImageView setAlpha:0];
		}
	}
}

+ (ODMPageControlViewCell *)cellWithIdentifier:(NSString *)identifier
{
	return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ODMPageControlViewCell class])
										  owner:self
										options:nil] lastObject];
}

+ (ODMPageControlViewCell *)cellWithIdentifier:(NSString *)identifier
										 title:(NSString *)title
										  icon:(UIImage *)icon
								   disabledIcon:(UIImage *)disabledIcon
{
	ODMPageControlViewCell *cell = [self cellWithIdentifier:identifier];
	cell.iconImageView.image = icon;
	cell.backgroundIconImageView.image = disabledIcon;
	cell.titleLabel.text = title;
	return cell;
}

@end
