//
//  ODMViewController.h
//  BouncingPageControl
//
//  Created by InICe on 11/6/13.
//  Copyright (c) 2013 Opendream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODMBouncingPageControl.h"

@interface ODMViewController : UIViewController {
	IBOutlet ODMBouncingPageControl *pageControl;
}

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end
